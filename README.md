# Tiny Pascal Compiler (PL0)

The code was taken from here:

http://pascal.hansotten.com/niklaus-wirth/pl0/

and adapted to compile with the Free Pascal Compiler (`fpc`) version 3.0.2. The
two examples were taken from:

https://en.wikipedia.org/wiki/PL/0

and adapted to work with the PL0 compiler.

## Build and Run

Execute:
```
./build
```

# References

[Compiler Construction](https://www.inf.ethz.ch/personal/wirth/CompilerConstruction/) by Niklaus Wirth 
